﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Web_ecomic.Models;

namespace Web_ecomic.Controllers
{
    public class XuliController : Controller
    {
        //
        // GET: /Xuli/
        private EcommicEntities3 db = new EcommicEntities3();
        public static List<Chitietphieu> gioHang = new List<Chitietphieu>();
        public ActionResult Xulidanhsachloaisanpham(int Idloai)
        {
            var danhsach = db.Sanphams.Where(m => m.ID_Loai == Idloai).ToList();
            ViewBag.run = 1;
            return View("Xulidanhsachloaisanpham",danhsach);
        }
        [HttpGet]
        public ActionResult AddToCart(string color, string quality)
        {
            try
            {
                int soluong = Int32.Parse(quality);
                if (soluong == 0)
                    soluong = 1;
                int temp = getctgiohang((int)T2V_Controller.current_sanpham.ID);
                if (temp == -1)
                {
                    Chitietphieu chitiet = new Chitietphieu();
                    chitiet.ID_sanpham = T2V_Controller.current_sanpham.ID;
                    chitiet.Soluong = soluong;
                    chitiet.color = color;
                    chitiet.Dongia = soluong * T2V_Controller.current_sanpham.Giatien;
                    chitiet.Sanpham = T2V_Controller.current_sanpham;
                    gioHang.Add(chitiet);
                }
                else
                {
                    if ((gioHang[temp].Soluong + soluong) <= T2V_Controller.current_sanpham.Soluong)
                    {
                        gioHang[temp].Soluong += soluong;
                        gioHang[temp].Dongia = gioHang[temp].Soluong * T2V_Controller.current_sanpham.Giatien;
                    }
                }
            }
            catch (Exception e) { }
            return RedirectToAction("Index", "T2V_");
        }
        public int getctgiohang(int a)
        {
            for(int i = 0 ; i < gioHang.Count ; i++)
            {
                if (gioHang[i].ID_sanpham == a)
                    return i;
            }
            return -1;
        }
        [HttpPost]
        public ActionResult Loaikhoigiohang(int a)
        {
            gioHang.RemoveAt(a);
            Session["tongtien"] = gioHang.Sum(m => m.Dongia).ToString();
            Session["soluong"] = gioHang.Sum(m => m.Soluong).ToString();
            Session["giohang"] = gioHang.Count;
            return View(gioHang);
        }
        [HttpPost]
        public ActionResult ChangeQuanity(string a, int b)
        {
            try
            {
                gioHang[b].Soluong = Int32.Parse(a);
                gioHang[b].Dongia = Int32.Parse(a) * gioHang[b].Sanpham.Giatien;
                Session["tongtien"] = XuliController.gioHang.Sum(m => m.Dongia).ToString();
                Session["soluong"] = XuliController.gioHang.Sum(m => m.Soluong).ToString();
            }
            catch (Exception e) { }
            return View("Loaikhoigiohang", gioHang);
        }
        [HttpPost]
        public JsonResult Changecolor(string a, int b)
        {
            try
            {
                gioHang[b].color = a;
            }
            catch (Exception e) { }
            return Json("changecolor", JsonRequestBehavior.AllowGet);
        }
        public ActionResult Deleteall()
        {
            gioHang.Clear();
            Session["tongtien"] = 0;
            Session["soluong"] = 0;
            Session["giohang"] = 0;
            return View("Loaikhoigiohang", gioHang);
        }
        public ActionResult Formthanhtoan()
        {
            return View(new Nguoinhan());
        }
        [HttpPost]
        public ActionResult Thanhtoan(Nguoinhan a, string ghichu)
        {
            
            try
            {
                if (gioHang.Count == 0)
                {
                    return RedirectToAction("Quanligiohang", "T2V_");
                }
                // save nguoi nhan
                db.Nguoinhans.Add(a);
                db.SaveChanges();
                int IDnguoinhan = a.ID;
                int IDnguoimua = T2V_Controller.Current_accounts.ID;
                Phieudathang phieu = new Phieudathang();
                phieu.Ghichu = ghichu;
                phieu.Trangthai = 1;
                phieu.Nguoinhan = IDnguoinhan;
                phieu.Nguoimua = IDnguoimua;
                phieu.Ngaymua = DateTime.Now;
                phieu.Soluong = gioHang.Count();
                phieu.Tongtien = gioHang.Sum(m => m.Dongia);
                db.Phieudathangs.Add(phieu);
                db.SaveChanges();
                int IDphieu = phieu.ID;
                for(int i = 0 ; i < gioHang.Count ; i++)
                {
                    gioHang[i].ID_phieu = IDphieu;
                    gioHang[i].Sanpham = null;
                    db.Chitietphieux.Add(gioHang[i]);
                }
                db.SaveChanges();
                gioHang.Clear();
            }
            catch (Exception e) { 
                ModelState.AddModelError("", "Da bi loi");
                return RedirectToAction("Quanligiohang", "T2V_");
            }
            return RedirectToAction("Index", "T2V_");
        }
        [HttpPost]
        public JsonResult Addtocart(int ID)
        {
            try
            {
                var temp1 = db.Sanphams.Where(m => m.ID == ID).ToList();
                int temp = getctgiohang(ID);
                if (temp == -1)
                {
                    Chitietphieu chitiet = new Chitietphieu();
                    chitiet.ID_sanpham = ID;
                    chitiet.Soluong = 1;
                    chitiet.color = "Trắng";
                    chitiet.Dongia = temp1[0].Giatien;
                    chitiet.Sanpham = temp1[0];
                    gioHang.Add(chitiet);
                }
                else
                {
                    if ((gioHang[temp].Soluong + 1) <= temp1[0].Soluong)
                    {
                        gioHang[temp].Soluong += 1;
                        gioHang[temp].Dongia = gioHang[temp].Soluong * temp1[0].Giatien;
                    }
                }
            }
            catch (Exception e) { }
            return Json(gioHang.Count,JsonRequestBehavior.AllowGet);
        }
        
    }
}
