﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Xml;
using Web_ecomic.Models;
using PagedList;
using System.Data.Entity;
using System.IO;
using System.Threading.Tasks;
using Microsoft.Owin.Host.SystemWeb;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Microsoft.AspNet.Identity;
using System.Drawing;
using System.Drawing.Text;
using System.Drawing.Drawing2D;
using System.Text;

namespace Web_ecomic.Controllers
{
    public class T2V_Controller : Controller
    {
        //
        // GET: /T2V_/
        public static WebModel data = null;
        private static WebModel datads = new WebModel(); 
        public static int CurrentID = 6;
        private EcommicEntities3 db = new EcommicEntities3();
        private static int id_Current;
        private static Comment cm;
        public static TaiKhoan Current_accounts;
        public static Sanpham current_sanpham;

        private ApplicationUserManager _userManager;
        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        private ApplicationSignInManager _signInManager;

        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set { _signInManager = value; }
        }

        public ActionResult Index()
        {
            if(Request.IsAuthenticated)
            {
                try
                {
                    Current_accounts = db.TaiKhoans.Where(m => m.Tentaikhoan == User.Identity.Name).ToList()[0];
                }
                catch (Exception e) { Current_accounts = new TaiKhoan(); }
            }
            if (data == null)
            {
                data = new WebModel();
                LoadData();

            }
            data.Tintucmoi = db.Tintucs.OrderByDescending(m => m.Ngay).Take(3).ToList();
            data.DSSanpham = db.Sanphams.ToList();
            data.sanPhamMoi = data.DSSanpham.OrderByDescending(m => m.Ngaydang).Take(3).ToList();
            data.DbLoaisanpham = db.LoaiSanPhams.ToList();

            Session["newproduct"] = data.DSSanpham.OrderByDescending(m => m.Ngaydang).Take(2).ToList();
            Session["menu"] = LoadMenu();
            
            Session.Add("giohang", XuliController.gioHang.Count);
            return View(data);
        }
        public List<Menupage> LoadMenu()
        {
            List<Menupage> ds = new List<Menupage>();
            int a;
            var menu = db.Menus.ToList();
            for(int i = 0 ; i < menu.Count ; i++)
            {
                Menupage temp = new Menupage();
                temp.ID = menu[i].ID;
                temp.Name = menu[i].Name;
                temp.Ds = db.Submenus.Where(m => m.Id_menu == temp.ID).ToList();
                ds.Add(temp);
            }
            return ds;
        }
        public void LoadData()
        {
            data.Slider.Add("images/slide1.jpg");
            data.Slider.Add("images/slide2.jpg");
            data.Slider.Add("images/slide3.jpg");
            data.Slider.Add("images/slide4.jpg");
            data.Slider.Add("images/slide5.jpg");
            data.Slider.Add("images/slide6.jpg");

            /*data.sanPhamMoi.Add(new SanPham(0, "Iphone6", "images/m1.jpg", 10, "Thong tin chi tiết sản phẩm", new List<string> { "../../images/Ip6/ip62.jpg", "../../images/Ip6/ip62.jpg", "../../images/Ip6/ip63.jpg", "../../images/Ip6/ip64.jpg" }));
            data.sanPhamMoi.Add(new SanPham(1, "MacBook Pro", "images/m2.jpg", 30, "Thong tin chi tiết sản phẩm", new List<string> { "../../images/m1.jpg", "../../images/Macbook/macbook1.jpg", "../../images/Macbook/macbook2.jpg" }));
            data.sanPhamMoi.Add(new SanPham(2, "Zenfone 2", "images/m4.jpg", 10, "Thong tin chi tiết sản phẩm", new List<string> { "../../images/m1.jpg", "s" }));

            data.SanphamShop.Add(new SanPham(3, "Ipad II", "images/a1.jpg", 10, "Thong tin chi tiết sản phẩm", new List<string> { "../../images/Ip6/ip61.jpg", "../../images/Ip6/ip62.jpg", "../../images/Ip6/ip63.jpg", "../../images/Ip6/ip64.jpg" }));
            data.SanphamShop.Add(new SanPham(4, "Ipad Mini", "images/a2.jpg", 6, "Thong tin chi tiết sản phẩm", new List<string> { "../../images/Ip6/ip61.jpg", "../../images/Ip6/ip62.jpg", "../../images/Ip6/ip63.jpg", "../../images/Ip6/ip64.jpg" }));
            data.SanphamShop.Add(new SanPham(5, "Iphone6 plus", "images/a4.jpg", 10, "Thong tin chi tiết sản phẩm", new List<string> { "../../images/Ip6/ip61.jpg", "../../images/Ip6/ip62.jpg", "../../images/Ip6/ip63.jpg", "../../images/Ip6/ip64.jpg" }));
            data.SanphamShop.Add(new SanPham(6, "MacBook Air", "images/a5.jpg", 25, "Thong tin chi tiết sản phẩm", new List<string> { "../../images/Ip6/ip61.jpg", "../../images/Ip6/ip62.jpg", "../../images/Ip6/ip63.jpg", "../../images/Ip6/ip64.jpg" }));
            */
            
        }

        public ActionResult Thongtinnhom()
        {
            return View();
        }

        public ActionResult SanPham()
        {
            DocDanhSachSanPham();
            return View(data);
        }

        private static List<Sanpham> gt = new List<Sanpham>();
        public ActionResult Danhsachsanpham(string name, string sapxep, int? page, string submenu)
        {
            if(name == "Tin tức")
            {
                return RedirectToAction("Danhsachtintuc");
            }
            bool temp = false;
            if (datads.isSort == false && (sapxep != null && datads.Sapxep != sapxep))
            {
                temp = true;
            }
            if (datads.isSort == false)
            {
                if (name != null)
                    datads.Loaisanpham = name;
                if (sapxep != null)
                    datads.Sapxep = sapxep;
                if (name == "Sản phẩm")
                {
                    if (datads.Sapxep == "Giá giảm")
                        gt = gt.OrderByDescending(n => n.Giatien).ToList();
                    else
                        if (datads.Sapxep == "Giá tăng")
                            gt = gt.OrderBy(n => n.Giatien).ToList();
                        else
                            gt = db.Sanphams.ToList();
                }
                else
                {
                    //data.DbLoaisanpham = db.LoaiSanPhams.Where(n => n.TenLoai == data.Loaisanpham).ToList();
                    int id = 0;
                    if (datads.Loaisanpham != "tk" && datads.Loaisanpham != null)
                    {
                        var tam = db.LoaiSanPhams.Where(n => n.TenLoai == datads.Loaisanpham).ToList();
                        if(tam.Count >= 1)
                            id = tam[0].ID;
                    }
                    if (datads.Sapxep == "Giá giảm")
                        gt = gt.OrderByDescending(n => n.Giatien).ToList();
                    else
                    {
                        if (datads.Sapxep == "Giá tăng")
                            gt = gt.OrderBy(n => n.Giatien).ToList();
                        else
                        {
                            if (datads.Loaisanpham == "tk")
                            { }
                            else
                            {
                                if (submenu != null)
                                {
                                    gt = db.Sanphams.Where(m => m.ID_Loai == id && m.NSX == submenu).ToList();
                                }
                                else
                                    gt = db.Sanphams.Where(n => n.ID_Loai == id).ToList();
                            }
                        }
                    }
                }
            }
            else
            {
                if (name == "Sản phẩm")
                {
                    if (datads.Sapxep == "Giá giảm")
                        gt = gt.OrderByDescending(n => n.Giatien).ToList();
                    else
                    {
                        if (datads.Sapxep == "Giá tăng")
                            gt = gt.OrderBy(n => n.Giatien).ToList();
                        else
                        {
                            if (datads.Sapxep == "Giá tăng")
                                gt = gt.OrderBy(n => n.Giatien).ToList();
                            else
                                gt = db.Sanphams.ToList();
                        }
                    }
                }
                else
                {
                    // data.DbLoaisanpham = db.LoaiSanPhams.Where(n => n.TenLoai == data.Loaisanpham).ToList();
                    int id = 0;
                    if (datads.Loaisanpham != "tk" && datads.Loaisanpham != null)
                    {
                        var tam = db.LoaiSanPhams.Where(n => n.TenLoai == datads.Loaisanpham).ToList();
                        if (tam.Count >= 1)
                            id = tam[0].ID;
                    }
                    if (datads.Sapxep == "Giá giảm")
                        gt = gt.OrderByDescending(n => n.Giatien).ToList();
                    else
                        if (datads.Sapxep == "Giá tăng")
                            gt = gt.OrderBy(n => n.Giatien).ToList();
                        else
                        {
                            if (datads.Loaisanpham == "tk")
                            { }
                            else
                            {
                                if (submenu != null)
                                {
                                    gt = db.Sanphams.Where(m => m.ID_Loai == id && m.NSX == submenu).ToList();
                                }
                                else
                                    gt = db.Sanphams.Where(n => n.ID_Loai == id).ToList();
                            }
                        }
                }
            }
            datads.isSort = temp;
            int pageNumbercurrent = page ?? 1;
            datads.DbSanPham = gt.ToList().ToPagedList(pageNumbercurrent, datads.numberofpage);
            return View(datads);
        }

        private void DocDanhSachSanPham()
        {
            System.Xml.XmlDocument xmldoc = new System.Xml.XmlDocument();
            xmldoc.Load(HttpContext.Server.MapPath("~/App_Data/SanPham.xml"));
            System.Xml.XmlElement root = xmldoc.DocumentElement;
            System.Xml.XmlNodeList DS = root.ChildNodes;
            foreach (System.Xml.XmlNode node in DS)
            {
                CurrentID = CurrentID + 1;
                data.SanphamShop.Add(new SanPhamtutao(CurrentID, node.SelectSingleNode("Ten").InnerText, node.SelectSingleNode("Loai").InnerText, node.SelectSingleNode("Image").InnerText, Convert.ToInt32(node.SelectSingleNode("Gia").InnerText), "Thong tin chi tiết sản phẩm", null));
            }
        }
        // Tin tuc
        public ActionResult Danhsachtintuc(int? page)
        {
            int Numsize = db.Tintucs.Count();
            int pagesize = 5;
            int numpage = Numsize / pagesize + 1;
            int pageNumbercurrent = (page ?? 1);
            var ds = db.Tintucs.OrderByDescending(t => t.Ngay);
            ViewBag.pageNumber = numpage;
            ViewBag.page = pageNumbercurrent;
            return View(ds.ToPagedList(pageNumbercurrent, pagesize));
        }

        public ActionResult Chitiettintuc(int ID)
        {
            id_Current = ID;
            var cttintuc = db.Tintucs.Find(ID);
            cm = new Comment();
            // comment hien tai
            if (Current_accounts != null)
            {
                cm.Name = Current_accounts.Hoten;
                cm.Url_image = Current_accounts.Imagedaidien;
                cm.ID_loai = ID;
                cm.ID_Sanpham = null;
                Session.Add("read", "true");
            }
            else
            {
                Session.Add("read", "false");
                cm.Name = "";
                cm.Url_image = "images/chimuc.jpg";
                cm.ID_loai = ID;
                cm.ID_Sanpham = null;
            }

            // lay list comment paging
            var liscomment = db.Comments.Where(m => m.ID_loai == ID).OrderByDescending(m => m.NgayDang);
            int pagenumber = 1;
            Comment_List cmlist = new Comment_List();
            cmlist.paginglist = new PagingList(liscomment.ToList(), pagenumber, 8);
            cmlist.istintuc = 1;
            cmlist.ID = ID;
            // Data get partial view form comment and show ds Comment
            Session.Add("dscomment", cmlist);
            ViewData.Add("cm", cm);
            return View(cttintuc);
        }
        [HttpPost]
        public ActionResult Comment(Comment tk)
        {
            tk.NgayDang = DateTime.Now;
            cm = tk;
            if (ModelState.IsValid)
            {
                db.Comments.Add(tk);
                db.SaveChanges();
            }
            Comment_List cmlist = new Comment_List();
            if (cm.ID_loai != null)
            {
                cmlist.paginglist = new PagingList(db.Comments.Where(m => m.ID_loai == tk.ID_loai).OrderByDescending(m => m.NgayDang).ToList(), 1, 8);
                cmlist.istintuc = 1;
                cmlist.ID = (int)tk.ID_loai;
            }
            else
            {
                cmlist.paginglist = new PagingList(db.Comments.Where(m => m.ID_Sanpham == tk.ID_Sanpham).OrderByDescending(m => m.NgayDang).ToList(), 1, 8);
                cmlist.istintuc = 0;
                cmlist.ID = (int)tk.ID_Sanpham;
            }

            return View("Ds_Comment", cmlist);
        }
        [HttpPost]
        public ActionResult phantrang(int istintuc, int ID, int page)
        {
            Comment_List cmlist = new Comment_List();
            if (istintuc == 1)
            {
                cmlist.paginglist = new PagingList(db.Comments.Where(m => m.ID_loai == ID).OrderByDescending(m => m.NgayDang).ToList(), page, 8);
                cmlist.istintuc = 1;
                cmlist.ID = ID;
            }
            else
            {
                cmlist.paginglist = new PagingList(db.Comments.Where(m => m.ID_Sanpham == ID).OrderByDescending(m => m.NgayDang).ToList(), page, 8);
                cmlist.istintuc = 0;
                cmlist.ID = ID;
            }

            return View("Ds_Comment", cmlist);
        }

        // Tai khoan

        public async Task<ActionResult> Dangnhap(LoginViewModel model, string returnUrl)
        {

            // This doen't count login failures towards lockout only two factor authentication
            // To enable password failures to trigger lockout, change to shouldLockout: true
            var result = await SignInManager.PasswordSignInAsync(model.Email, model.Password, model.RememberMe, shouldLockout: false);
            switch (result)
            {
                case SignInStatus.Success:
                    {
                        try
                        {
                            Current_accounts = db.TaiKhoans.Where(m => m.Tentaikhoan == model.Email).ToList()[0];
                            Session["login"] = null;
                            Session["Loi"] = null;
                        }
                        catch (Exception e) { }
                        return RedirectToAction("Index");
                    }
                case SignInStatus.LockedOut:
                    return RedirectToAction("Index");
                case SignInStatus.RequiresVerification:
                    return RedirectToAction("Index");
                case SignInStatus.Failure:
                default:
                    {
                        ModelState.AddModelError("", "Invalid login attempt.");
                        Session["login"] = model;
                        Session["Loi"] = "Usename or password is incorrect";
                        return RedirectToAction("Index");
                    }
            }
        }
        public async Task<ActionResult> Dangki(RegisterViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = new ApplicationUser { UserName = model.Email, Email = model.Email };
                if (Session["Captcha"] == null || Session["Captcha"].ToString() != model.Captcha)
                {
                    ModelState.AddModelError("Captcha", "Wrong value of sum, please try again.");
                    Session["dangki"] = model;
                    Session["Loi"] = "Wrong value of sum, please try again.";
                    return RedirectToAction("Index");
                }
                
               
                var result = await UserManager.CreateAsync(user, model.Password);
                if (result.Succeeded)
                {
                    try
                    {
                        
                        await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
                        TaiKhoan temp = new TaiKhoan();
                        temp.Tentaikhoan = user.Email;
                        temp.Email = user.Email;
                        temp.Hoten = user.Email;
                        db.TaiKhoans.Add(temp);
                        db.SaveChanges();
                        Current_accounts = temp;
                        Session["Loi"] = null;
                        Session["dangki"] = null;
                        Guimail(user.Email);
                        return RedirectToAction("Index");
                    }
                    catch (Exception e) { }
                }
               
                AddErrors(result);
                Session["dangki"] = model;
                Session["Loi"] = result.Errors.First();
                return RedirectToAction("Index");
            }
            return RedirectToAction("Index");
        }

        public void Guimail(string Email)
        {
            string smtpUsername = "vuong01699219217@gmail.com";
            string smtpPass = "vuongdaica";
            string smtpHost = "smtp.gmail.com";
            int smtpPort = 25;
            string emailTo = Email;
            string chude = "Kich hoat Tai Khoan";
            string body = "Ban da dang ki thanh cong tai khoan cua minh tren trang web mua sam cua chung toi";
            EmailServer sever = new EmailServer();
            bool kt = sever.send(smtpUsername, smtpPass, smtpHost, smtpPort, emailTo, chude, body);

            if (kt) ModelState.AddModelError("", "Da dang ki thanh cong, vui long kt mail");
            else ModelState.AddModelError("", " dang ki that bai");
        }
        public ActionResult CaptchaImage(string prefix, bool noisy = true)
        {
            var rand = new Random((int)DateTime.Now.Ticks);
            //generate new question 
            int a = rand.Next(10, 99);
            int b = rand.Next(0, 9);
            var captcha = string.Format("{0} + {1} = ?", a, b);

            //store answer 
            Session["Captcha" + prefix] = a + b;

            //image stream 
            FileContentResult img = null;

            using (var mem = new MemoryStream())
            using (var bmp = new Bitmap(130, 30))
            using (var gfx = Graphics.FromImage((Image)bmp))
            {
                gfx.TextRenderingHint = TextRenderingHint.ClearTypeGridFit;
                gfx.SmoothingMode = SmoothingMode.AntiAlias;
                gfx.FillRectangle(Brushes.White, new Rectangle(0, 0, bmp.Width, bmp.Height));

                //add noise 
                if (noisy)
                {
                    int i, r, x, y;
                    var pen = new Pen(System.Drawing.Color.Yellow);
                    for (i = 1; i < 10; i++)
                    {
                        pen.Color = System.Drawing.Color.FromArgb(
                        (rand.Next(0, 255)),
                        (rand.Next(0, 255)),
                        (rand.Next(0, 255)));

                        r = rand.Next(0, (130 / 3));
                        x = rand.Next(0, 130);
                        y = rand.Next(0, 30);

                        gfx.DrawEllipse(pen, x - r, y - r, r, r);
                    }
                }

                //add question 
                gfx.DrawString(captcha, new Font("Tahoma", 15), Brushes.Gray, 2, 3);

                //render as Jpeg 
                bmp.Save(mem, System.Drawing.Imaging.ImageFormat.Jpeg);
                img = this.File(mem.GetBuffer(), "image/Jpeg");
            }

            return img;
        }
        public ActionResult Quenmatkhau ()
        {
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Quenmatkhau(ForgotPasswordViewModel model)
        {
            var user = await UserManager.FindByNameAsync(model.Email);
            string a = RandomString();
            string resetToken = await UserManager.GeneratePasswordResetTokenAsync(user.Id);
            var passwordChangeResult = await UserManager.ResetPasswordAsync(user.Id, resetToken, a);
            Guimail1(model.Email, a);
            return RedirectToAction("Index");
        }
        private string RandomString()
        {
            StringBuilder sb = new StringBuilder();
            char c;
            Random rand = new Random();
            for (int i = 0; i < 10; i++)
            {
                c = Convert.ToChar(Convert.ToInt32(rand.Next(65, 87)));
                sb.Append(c);
            }
            string a = "V1@";

            return sb.ToString().ToLower() + a;

        }
        public void Guimail1(string Email, string mk)
        {
            string smtpUsername = "vuong01699219217@gmail.com";
            string smtpPass = "vuongdaica";
            string smtpHost = "smtp.gmail.com";
            int smtpPort = 25;
            string emailTo = Email;
            string chude = "Mat khau cua ban la :";
            string body = mk;
            EmailServer sever = new EmailServer();
            bool kt = sever.send(smtpUsername, smtpPass, smtpHost, smtpPort, emailTo, chude, body);

            if (kt) ModelState.AddModelError("", "Da dang ki thanh cong, vui long kt mail");
            else ModelState.AddModelError("", " dang ki that bai");
        }
        // Đăng xuất
        public ActionResult Dangxuat(Comment tk)
        {
            AuthenticationManager.SignOut();
            Current_accounts = null;
            return RedirectToAction("Index");
        }
        // Thong tin ca nhan
        public ActionResult Thongtincanhan(string message)
        {
            Current_accounts = db.TaiKhoans.Find(Current_accounts.ID);
            ViewBag.notify = message;
            return View(Current_accounts);
        }
        public ActionResult Chinhsuattcanhan()
        {
            Current_accounts = db.TaiKhoans.Find(Current_accounts.ID);    
            return View(Current_accounts);
        }
        [HttpPost]
        public ActionResult Chinhsuattcanhan(TaiKhoan tk, HttpPostedFileBase file)
        {
            if (file != null)
            {
                string path = SaveImage(file);
                tk.Imagedaidien = "images/" + path;
            }
            if (ModelState.IsValid)
            {
                db.Entry(tk).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Thongtincanhan");
            }

            return View(Current_accounts);
        }
        public string SaveImage(HttpPostedFileBase file)
        {
            string path = string.Empty;
            if (file != null && file.ContentLength > 0)
            {
                try
                {
                    path = Path.Combine(Server.MapPath("~/images"), Path.GetFileName(file.FileName));
                    file.SaveAs(path);
                    ViewBag.Message = "File uploaded successfully";
                }
                catch (Exception ex)
                {
                    ViewBag.Message = "ERROR:" + ex.Message.ToString();
                }
            }
            else
            {
                ViewBag.Message = "You have not specified a file.";
            }
            return Path.GetFileName(file.FileName);
        }
        [ValidateInput(false)]
        public ActionResult Thongtinchitietsanpham(Sanpham sp)
        {
            id_Current = sp.ID;

            // comment hien tai, check accounts
            cm = new Comment();
            if (Request.IsAuthenticated)
            {
                cm.Name = Current_accounts.Hoten;
                cm.Url_image = Current_accounts.Imagedaidien;
                cm.ID_loai = null;
                cm.ID_Sanpham = sp.ID;
                Session.Add("read", "true");
            }
            else
            {
                Session.Add("read", "false");
                cm.Name = "";
                cm.Url_image = "images/chimuc.jpg";
                cm.ID_loai = null;
                cm.ID_Sanpham = sp.ID;
            }

            var liscomment = db.Comments.Where(m => m.ID_Sanpham == sp.ID).OrderByDescending(m => m.NgayDang);
            int pagenumber = 1;
            Comment_List cmlist = new Comment_List();
            cmlist.paginglist = new PagingList(liscomment.ToList(), pagenumber, 8);
            cmlist.istintuc = 0;
            cmlist.ID = sp.ID;
            // Data get partial view form comment and show ds Comment
            Session.Add("dscomment", cmlist);
            ViewData.Add("cm", cm);

            sp.Colors = db.Colors.Where(m => m.ID_sanpham == sp.ID).ToList();
            //Current san pham
            Chitietsanpham tam = new Chitietsanpham();
            tam.sanpham = sp;
            tam.Sanphamlienquan = db.Sanphams.Where(m => m.Giatien < (sp.Giatien + 2000000) & m.Giatien > (sp.Giatien - 2000000) & (m.ID != sp.ID)).Take(5).ToList();
            
            current_sanpham = sp;
            @Session["giohang"] = XuliController.gioHang.Count;
            return View(tam);
        }

        public ActionResult Quanligiohang()
        {
            Session["tongtien"] = XuliController.gioHang.Sum(m => m.Dongia).ToString();
            Session["soluong"] = XuliController.gioHang.Sum(m => m.Soluong).ToString();
            Session["giohang"] = XuliController.gioHang.Count;
            if(Request.IsAuthenticated)
            {
                Session["islogin"] = "Y";
            }
            else
                Session["islogin"] = "N";
            return View(XuliController.gioHang);
        }
        public ActionResult Timkiem(string search)
        {
            string[] temp = search.Split('-');
            if (temp.Length <= 1)
            {
                gt = data.Timkiemcoban(search);
            }
            else
            {
                gt = data.Timkiemnangcao(temp);
            }
            datads.DbSanPham = gt.ToList().ToPagedList(1, datads.numberofpage);
            datads.Loaisanpham = "tk";
            return View("Danhsachsanpham", datads);
        }
        public ActionResult Sanphammoira()
        {
            DateTime pass = DateTime.Now.AddDays(-7);
            gt = db.Sanphams.Where(m => m.Ngaydang <= DateTime.Now & m.Ngaydang >= pass).ToList();
            datads.DbSanPham = gt.ToList().ToPagedList(1, datads.numberofpage);
            datads.Loaisanpham = "tk";
            return View("Danhsachsanpham", datads);
        }
        public ActionResult ChangePassword()
        {
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ChangePassword(ChangePasswordViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            var result = await UserManager.ChangePasswordAsync(User.Identity.GetUserId(), model.OldPassword, model.Password);
            if (result.Succeeded)
            {
                var user = await UserManager.FindByIdAsync(User.Identity.GetUserId());
                if (user != null)
                {
                    await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
                }
                return RedirectToAction("Thongtincanhan", new { Message = "Change password succeeded" });
            }
            AddErrors(result);
            return View(model);
        }
        public JsonResult Hotrodangki_dn()
        {
            Session["dangki"] = null;
            Session["Loi"] = null;
            Session["login"] = null;
            return Json("success", JsonRequestBehavior.AllowGet);
        }

        public ActionResult Danhsachphieu()
        {
            var ds = db.Phieudathangs.Where(m => m.Nguoimua == Current_accounts.ID).OrderByDescending(m=>m.Ngaymua).ToList();
            List<Phieu> danhsachphieu = new List<Phieu>();
            for (int i = 0; i < ds.Count; i++ )
            {
                Phieu temp = new Phieu();
                temp.IDphieu = ds[i].ID;
                temp.Tennguoimua = Current_accounts.Hoten;
                temp.Tennguoinhan = db.Nguoinhans.Find(ds[i].Nguoinhan).Hoten;
                temp.ngaymua = (DateTime)ds[i].Ngaymua;
                temp.Soluong = (int)ds[i].Soluong;
                temp.Tongtien = (Double)ds[i].Tongtien;
                temp.Ghichu = ds[i].Ghichu;
                temp.Trangthai = (int)ds[i].Trangthai;
                danhsachphieu.Add(temp);
            }
            return View(danhsachphieu);
        }

        public ActionResult Chitietphieu(int IDPhieu)
        {
            var ds = db.Chitietphieux.Where(m => m.ID_phieu == IDPhieu).ToList();
            return View(ds);
        }
        #region Helpers
        // Used for XSRF protection when adding external logins
        private const string XsrfKey = "XsrfId";

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            return RedirectToAction("Index", "Home");
        }

        internal class ChallengeResult : HttpUnauthorizedResult
        {
            public ChallengeResult(string provider, string redirectUri)
                : this(provider, redirectUri, null)
            {
            }

            public ChallengeResult(string provider, string redirectUri, string userId)
            {
                LoginProvider = provider;
                RedirectUri = redirectUri;
                UserId = userId;
            }

            public string LoginProvider { get; set; }
            public string RedirectUri { get; set; }
            public string UserId { get; set; }

            public override void ExecuteResult(ControllerContext context)
            {
                var properties = new AuthenticationProperties { RedirectUri = RedirectUri };
                if (UserId != null)
                {
                    properties.Dictionary[XsrfKey] = UserId;
                }
                context.HttpContext.GetOwinContext().Authentication.Challenge(properties, LoginProvider);
            }
        }
        #endregion

    }
}

