﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Net;
using System.Web.Mvc;
using Web_ecomic.Models;
using PagedList;
using Microsoft.Owin.Host.SystemWeb;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Microsoft.AspNet.Identity;
using System.Threading.Tasks;

namespace Web_ecomic.Controllers
{
    [Authorize(Roles = "Admin")]
    public class AdminController : Controller
    {
        //
        // GET: /Admin/

        public EcommicEntities3 db = new EcommicEntities3();
        public Sanpham data = null;
        public static WebModel WMdata = null;

        private ApplicationUserManager _userManager;
        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        private ApplicationSignInManager _signInManager;

        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set { _signInManager = value; }
        }

        private ApplicationRoleManager _roleManager;
        public ApplicationRoleManager RoleManager
        {
            get
            {
                return _roleManager ?? HttpContext.GetOwinContext().Get<ApplicationRoleManager>();
            }
            private set
            {
                _roleManager = value;
            }
        }

        public ActionResult Index()
        {
            if (WMdata == null)
                WMdata = new WebModel();
            if (data == null)
                data = new Models.Sanpham();
            WMdata.comments = db.Comments.Count();
            WMdata.Orders = db.Phieudathangs.Count();
            DateTime ngay = DateTime.Now.AddDays(-1);
            DateTime tuan = DateTime.Now.AddDays(-7);
            DateTime thang = DateTime.Now.AddMonths(-1);
            DateTime quy = DateTime.Now.AddMonths(-3);
            DateTime nam = DateTime.Now.AddYears(-1);
            WMdata.doanhsotheongay = db.Phieudathangs.Where(m => m.Ngaymua >= ngay).Sum(m => m.Tongtien);
            WMdata.doanhsotheotuan = db.Phieudathangs.Where(m => m.Ngaymua >= tuan).Sum(m => m.Tongtien);
            WMdata.doanhsotheothang = db.Phieudathangs.Where(m => m.Ngaymua >= thang).Sum(m => m.Tongtien);
            WMdata.doanhsotheoquy = db.Phieudathangs.Where(m => m.Ngaymua >= quy).Sum(m => m.Tongtien);
            WMdata.doanhsotheonam = db.Phieudathangs.Where(m => m.Ngaymua >= nam).Sum(m => m.Tongtien);

            Dictionary<int?, int?> thongtin = new Dictionary<int?, int?>();
            var temp = db.Chitietphieux.GroupBy(m => m.ID_sanpham);
            int t = temp.Count();
            foreach (var dbitem in temp)
            {
                Chitietphieu item = dbitem.First();
                int? tong = db.Chitietphieux.Where(m => m.ID_sanpham == item.ID_sanpham).Sum(m => m.Soluong);
                thongtin.Add(item.ID_sanpham, tong);
            }
            var items = from item in thongtin orderby item.Value descending select item;

            WMdata.top10 = new List<string>();
            int stop = 0;
            foreach(var item in items)
            {
                var sp = db.Sanphams.Where(m => m.ID == item.Key).First();
                WMdata.top10.Add(sp.TenSanPham);
                stop++;
                if (stop == 10)
                    break;
            }

            //var model = "vanty";
            //TO DO:
            return View("Index", "AdminLayout", WMdata);
        }
        public ActionResult Themmoi()
        {
            Tintuc c = new Tintuc();
            return View(c);
        }
        [HttpPost, ValidateInput(false)]
        // Them moi tin tuc
        public ActionResult Themmoi(Tintuc tintuc, HttpPostedFileBase file)
        {
            string path = string.Empty;
            if (file != null && file.ContentLength > 0)
            {
                try
                {
                    path = Path.Combine(Server.MapPath("~/images"), Path.GetFileName(file.FileName));
                    file.SaveAs(path);
                    ViewBag.Message = "File uploaded successfully";
                    tintuc.Imageurl = "images/" + Path.GetFileName(file.FileName);
                }
                catch (Exception ex)
                {
                    ViewBag.Message = "ERROR:" + ex.Message.ToString();
                    tintuc.Imageurl = "";
                }
            }
            else
            {
                ViewBag.Message = "You have not specified a file.";
            }


            tintuc.Ngay = DateTime.Now;
            if (ModelState.IsValid)
            {
                db.Tintucs.Add(tintuc);
                db.SaveChanges();
                return RedirectToAction("Danhsachtintuc");
            }
            return View();
        }

        public ActionResult Danhsachtintuc(int? page)
        {
            int Numsize = db.Tintucs.Count();
            int pagesize = 5;
            int numpage = Numsize / pagesize + 1;
            int pageNumbercurrent = (page ?? 1);
            var ds = db.Tintucs.OrderByDescending(t => t.Ngay);
            ViewBag.pageNumber = numpage;
            ViewBag.page = pageNumbercurrent;
            return View(ds.ToPagedList(pageNumbercurrent, pagesize));
        }
        // Delete tin tuc
        public ActionResult Delete(int ID)
        {
            Tintuc tt = db.Tintucs.Find(ID);
            if (tt == null)
            {
                HttpNotFound();
            }
            try
            {
                db.Tintucs.Remove(tt);
                db.Comments.RemoveRange(db.Comments.Where(c => c.ID_loai == ID));
                db.SaveChangesAsync();
            }
            catch (Exception e)
            {

            }

            return RedirectToAction("Danhsachtintuc");
        }
        // Edit tin tuc
        public ActionResult Edit(int ID)
        {
            Tintuc tt = db.Tintucs.Find(ID);
            if (tt == null)
            {
                HttpNotFound();
            }
            return View(tt);
        }

        // khac phuc loi text include html, script
        [HttpPost, ValidateInput(false)]
        public ActionResult Edit(Tintuc tintuc, HttpPostedFileBase file)
        {
            if (file != null)
            {
                string path = SaveImage(file);
                tintuc.Imageurl = "images/" + path;
            }
            tintuc.Ngay = DateTime.Now;
            if (ModelState.IsValid)
            {
                db.Entry(tintuc).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Danhsachtintuc");
            }
            return View(tintuc);
        }
        // Save hinh
        public string SaveImage(HttpPostedFileBase file)
        {
            string path = string.Empty;
            if (file != null && file.ContentLength > 0)
            {
                try
                {
                    path = Path.Combine(Server.MapPath("~/images"), Path.GetFileName(file.FileName));
                    file.SaveAs(path);
                    ViewBag.Message = "File uploaded successfully";
                }
                catch (Exception ex)
                {
                    ViewBag.Message = "ERROR:" + ex.Message.ToString();
                }
            }
            else
            {
                ViewBag.Message = "You have not specified a file.";
            }
            return Path.GetFileName(file.FileName);
        }

        public ActionResult AllComment(int? page)
        {
            var Ds_Comment = db.Comments.OrderByDescending(m => m.NgayDang);
            int pagesize = 8;
            int pageNumbercurrent = (page ?? 1);// neu page == null thi pageNumbercurrent = 1 ? pageNumbercurrent = page
            return View(Ds_Comment.ToPagedList(pageNumbercurrent, pagesize));
        }
        public ActionResult DeleteComment(int ID)
        {
            Comment tt = db.Comments.Find(ID);
            if (tt == null)
            {
                HttpNotFound();
            }
            db.Comments.Remove(tt);
            db.SaveChanges();
            return RedirectToAction("AllComment");
        }

        public ActionResult Themmoisanpham()
        {
            if (data == null)
                data = new Models.Sanpham();
            data.DbLoaisanpham = db.LoaiSanPhams.ToList();
            return View(data);
        }

        [HttpPost, ValidateInput(false)]
        // Them moi tin tuc
        public ActionResult Themmoisanpham(Sanpham Sanpham, HttpPostedFileBase file, string color)
        {
            string path = string.Empty;
            if (file != null && file.ContentLength > 0)
            {
                try
                {
                    path = Path.Combine(Server.MapPath("~/images"), Path.GetFileName(file.FileName));
                    file.SaveAs(path);
                    ViewBag.Message = "File uploaded successfully";
                    Sanpham.Duongdanhinh = "images/" + Path.GetFileName(file.FileName);
                }
                catch (Exception ex)
                {
                    ViewBag.Message = "ERROR:" + ex.Message.ToString();
                }
            }
            else
            {
                ViewBag.Message = "You have not specified a file.";
            }

            if (ModelState.IsValid)
            {
                Sanpham.Ngaydang = DateTime.Now;
                db.Sanphams.Add(Sanpham);
                db.SaveChanges();
                string[] colors = color.Split(';');
                for (int i = 0;i < colors.Length; i++ )
                {
                    Color tam = new Color();
                    tam.Ten = colors[i];
                    tam.ID_sanpham = Sanpham.ID;
                    db.Colors.Add(tam);
                }
                db.SaveChanges();
                return RedirectToAction("Danhsachsanpham");
            }
            if (data == null)
                data = new Models.Sanpham();
            data.DbLoaisanpham = db.LoaiSanPhams.ToList();
            return View(data);
        }

        public ActionResult Danhsachsanpham(string sapxep, int? page)
        {
            if (WMdata == null)
                WMdata = new WebModel();
            var gt = db.Sanphams.ToList();
            if (WMdata.isSort == false)
            {
                if (sapxep != null)
                    WMdata.Sapxep = sapxep;
                if (WMdata.Sapxep == "Giá giảm")
                    gt = db.Sanphams.OrderByDescending(n => n.Giatien).ToList();
                else
                    if (WMdata.Sapxep == "Giá tăng")
                        gt = db.Sanphams.OrderBy(n => n.Giatien).ToList();
                WMdata.isSort = true;
            }
            else
            {
                if (WMdata.Sapxep == "Giá giảm")
                    gt = db.Sanphams.OrderByDescending(n => n.Giatien).ToList();
                else
                    if (WMdata.Sapxep == "Giá tăng")
                        gt = db.Sanphams.OrderBy(n => n.Giatien).ToList();
                WMdata.isSort = false;
            }
            if (sapxep == null)
                WMdata.isSort = false;
            if (page != null)
                WMdata.isSort = false;
            int pageNumbercurrent = page ?? 1;

            WMdata.DbSanPham = gt.ToList().ToPagedList(pageNumbercurrent, WMdata.numberofpage);
            return View(WMdata);
        }

        public ActionResult XoaSanpham(int ID)
        {
            Sanpham sp = db.Sanphams.Find(ID);
            if (sp == null)
            {
                HttpNotFound();
            }
            try
            {
                db.Sanphams.Remove(sp);
                db.SaveChangesAsync();
            }
            catch (Exception e)
            {

            }

            return RedirectToAction("Danhsachsanpham");
        }

        public ActionResult Chinhsuasanpham(int ID)
        {
            Sanpham sp = db.Sanphams.Find(ID);
            sp.DbLoaisanpham = db.LoaiSanPhams.ToList();
            if (sp == null)
            {
                HttpNotFound();
            }
            return View(sp);
        }

        // khac phuc loi text include html, script
        [HttpPost, ValidateInput(false)]
        public ActionResult Chinhsuasanpham(Sanpham sp, HttpPostedFileBase file)
        {
            sp.DbLoaisanpham = db.LoaiSanPhams.ToList();
            string path = string.Empty;
            if (file != null && file.ContentLength > 0)
            {
                try
                {
                    path = Path.Combine(Server.MapPath("~/images"), Path.GetFileName(file.FileName));
                    file.SaveAs(path);
                    ViewBag.Message = "File uploaded successfully";
                    sp.Duongdanhinh = "images/" + Path.GetFileName(file.FileName);
                }
                catch (Exception ex)
                {
                    ViewBag.Message = "ERROR:" + ex.Message.ToString();
                }
            }
            else
            {
                ViewBag.Message = "You have not specified a file.";
            }
            if (ModelState.IsValid)
            {
                sp.Ngaydang = DateTime.Now;
                db.Entry(sp).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Danhsachsanpham");
            }
            return View(sp);
        }

        public ActionResult Themmoiloaisanpham()
        {
            return View();
        }

        [HttpPost, ValidateInput(false)]
        // Them moi tin tuc
        public ActionResult Themmoiloaisanpham(LoaiSanPham loai)
        {
            if (ModelState.IsValid)
            {
                db.LoaiSanPhams.Add(loai);
                db.SaveChanges();
                return RedirectToAction("Danhsachloaisanpham");
            }
            return View();
        }

        public ActionResult Danhsachloaisanpham()
        {
            LoaiSanPham loai = new Models.LoaiSanPham();
            loai.DbLoaisanpham = db.LoaiSanPhams.ToList();
            return View(loai);
        }

        [HttpPost, ValidateInput(false)]
        public ActionResult Danhsachloaisanpham(LoaiSanPham loaisp, string Command)
        {
            LoaiSanPham loai = new Models.LoaiSanPham();
            loai.DbLoaisanpham = db.LoaiSanPhams.ToList();
            List<LoaiSanPham> temp = new List<LoaiSanPham>();
            temp = db.LoaiSanPhams.Where(n => n.TenLoai == loaisp.TenLoai).ToList();
            int id = temp[0].ID;
            loaisp.ID = id;
            if (Command == "Edit")
            {
                return RedirectToAction("Chinhsualoaisanpham", loaisp);
            }
            else
            {
                if (Command == "Delete")
                {
                    IEnumerable<Sanpham> dssp;
                    dssp = db.Sanphams.Where(n => n.ID_Loai == loaisp.ID);
                    db.Sanphams.RemoveRange(dssp);
                    LoaiSanPham loaispDel = db.LoaiSanPhams.Find(id);
                    if (loaispDel == null)
                    {
                        HttpNotFound();
                    }
                    try
                    {
                        db.LoaiSanPhams.Remove(loaispDel);
                        db.SaveChangesAsync();
                    }
                    catch (Exception e)
                    { }
                    return RedirectToAction("Danhsachloaisanpham");
                }
            }
            return View(loai);
        }

        public ActionResult Chinhsualoaisanpham(LoaiSanPham loaisp)
        {
            LoaiSanPham sp = db.LoaiSanPhams.Find(loaisp.ID);
            return View(sp);
        }

        [HttpPost, ValidateInput(false)]
        // Them moi tin tuc
        public ActionResult Chinhsualoaisanpham(LoaiSanPham loaisp, string Command)
        {
            if (ModelState.IsValid)
            {
                db.Entry(loaisp).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Danhsachloaisanpham");
            }
            return View();
        }

        private static Danhsachtaikhoan dstk = new Danhsachtaikhoan();
        private static ListUser dsuser = new ListUser();

        public ActionResult Danhsachtaikhoan(int? page, string sapxep)
        {

            var tk = db.TaiKhoans.OrderByDescending(m => m.Ngaydangki);
            int pagenumber = page ?? 1;
            //dstk.ds_taikhoan = tk.ToPagedList(pagenumber, 5);
            //var list = UserManager.Users.ToList();
            var list = db.TaiKhoans.ToList();
            dsuser.ds_id = new List<string>();
            foreach(TaiKhoan item in list)
            {
                string id = UserManager.FindByEmail(item.Email).Id;
                dsuser.ds_id.Add(id);
            }
            dsuser.ds_thongtintaikhoan = db.TaiKhoans.ToList().ToPagedList(pagenumber, 5);
            return View(dsuser);
        }

        public ActionResult Chitiettaikhoan(string id)
        {
            var user = UserManager.FindById(id);
            TaiKhoan temp = db.TaiKhoans.Where(m => m.Tentaikhoan == user.Email).ToList()[0];
            var role = RoleManager.FindByName("Admin");
            temp.IsAdmin = UserManager.IsInRole(id, role.Name);
            return View(temp);
        }

        [HttpPost]
        public ActionResult Chitiettaikhoan(TaiKhoan tk, HttpPostedFileBase file, string check)
        {
            if (file != null)
            {
                string path = SaveImage(file);
                tk.Imagedaidien = "images/" + path;
            }
            if (ModelState.IsValid)
            {
                db.Entry(tk).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
            }
            if (check == "Admin")
            {
                var user = UserManager.FindByEmail(tk.Email);
                var role = RoleManager.FindByName("Admin");
                var result = UserManager.AddToRole(user.Id, role.Name);
                tk.IsAdmin = true;
            }
            else
            {
                var user = UserManager.FindByEmail(tk.Email);
                var result = UserManager.RemoveFromRole(user.Id, "Admin");
                tk.IsAdmin = false;
            }
            return View(tk);
        }

        public async Task<ActionResult> Phanquyentaikhoan(int? page, string sapxep)
        {
            var role = RoleManager.FindByName("Admin");
            // Get the list of Users in this Role
            var users = new List<ApplicationUser>();

            // Get the list of Users in this Role
            foreach (var user in UserManager.Users.ToList())
            {
                if (await UserManager.IsInRoleAsync(user.Id, role.Name))
                {
                    users.Add(user);
                }
            }
            Phanquyen phanquyen = new Phanquyen();
            int pagenumber = page ?? 1;
            phanquyen.ds_taikhoan = users.ToPagedList(pagenumber, 5);
            return View(phanquyen);
        }

        public ActionResult Xoaquyenadmin(string id)
        {
            var result = UserManager.RemoveFromRole(id, "Admin");
            return RedirectToAction("Phanquyentaikhoan");
        }

        public ActionResult Danhsachdonhang(int? page, string sapxep)
        {
            if (WMdata == null)
                WMdata = new WebModel();
            var phieu = db.Phieudathangs.ToList();
            int pagenumber = page ?? 1;
            WMdata.DbPhieudathang = phieu.ToPagedList(pagenumber, 20);
            return View(WMdata);
        }

        public ActionResult Doanhsotheothang()
        {
            System.DateTime date1 = new System.DateTime(1996, 6, 3, 22, 15, 0);
            db.Phieudathangs.Where(m => m.Ngaymua >= DateTime.Now.AddDays(-30));
            return View();
        }
    }
}
