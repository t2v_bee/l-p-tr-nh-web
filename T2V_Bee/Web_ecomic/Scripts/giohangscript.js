﻿function Loaibo(value) {
    var url = '@Url.Action("Loaikhoigiohang","Xuli")';
    $.ajax({
        url: url,
        type: 'POST',
        cache: false,
        data: { a: value },
        success: function (result) {
            $('.quanli').html(result.toString());
        },
    });
}
function changequantity(value, value1) {
    var url = '@Url.Action("ChangeQuanity", "Xuli")';
    $.ajax({
        url: url,
        type: 'POST',
        cache: false,
        data: { a: value, b: value1 },
        success: function (result) {
            $('.quanli').html(result.toString());
        },
    });
}
function changecolor(value, value1) {
    var url = '@Url.Action("Changecolor", "Xuli")';
    $.ajax({
        url: url,
        type: 'POST',
        cache: false,
        data: { a: value, b: value1 },
        success: function (result) {
        },
    });
}
function Deleteall() {
    var url = '@Url.Action("Deleteall", "Xuli")';
    $.ajax({
        url: url,
        type: 'POST',
        cache: false,
        data: {},
        success: function (result) {
            $('.quanli').html(result.toString());
            $('.last').html("0");
        },
    });
}
function Thanhtoanform() {
    var url = '@Url.Action("Formthanhtoan", "Xuli")';
    if(@Session["currenttk"] == false)
    {
        var login = $('.login1');
        var signup = $('.sign-up');
        var body = $('body');
        body.css({ 'overflow': 'hidden' });
        login.fadeIn(500)
    }
    else
    {
        $.ajax({
            url: url,
            type: 'POST',
            cache: false,
            data: {},
            success: function (result) {
                $('.form-tt').html(result.toString());
            },
        });
    }
}