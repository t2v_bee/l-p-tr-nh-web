﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PagedList;
using System.ComponentModel.DataAnnotations;

namespace Web_ecomic.Models
{
    public class WebModel
    {
        public List<string> Slider = new List<string>();
        public List<Sanpham>sanPhamMoi = new List<Sanpham>();
        public List<SanPhamtutao> SanphamShop = new List<SanPhamtutao>();
        public List<Tintuc> Tintucmoi;
        public Sanpham Currenproduct;

        public IPagedList<Sanpham> DbSanPham;
        public IPagedList<Phieudathang> DbPhieudathang;
        public List<LoaiSanPham> DbLoaisanpham = new List<LoaiSanPham>();
        public string Loaisanpham;
        public string Sapxep = "Còn hàng";
        public int numberofpage = 4;
        public bool isSort = false;
        public int comments;
        public int Orders;
        public double? doanhsotheongay;
        public double? doanhsotheotuan;
        public double? doanhsotheothang;
        public double? doanhsotheoquy;
        public double? doanhsotheonam;
        public List<Sanpham> DSSanpham = new List<Sanpham>();
        public List<string> top10 = new List<string>();

        public List<SanPhamtutao> LayDanhSachTheoLoaiSanPham(string loai)
        {
            List<SanPhamtutao> sanPham = new List<SanPhamtutao>();
            for(int i = 0; i < SanphamShop.Count; i++)
            {
                if (SanphamShop[i].Loai == loai)
                    sanPham.Add(SanphamShop[i]);
            }
            return sanPham;
        }
        public List<Sanpham>Timkiemcoban(string input)
        {
            List<Sanpham> Ds = Timkiemloai(input);
            if (Ds.Count > 0)
                return Ds;
            Ds = TimkiemNSX(input);
            if (Ds.Count > 0)
                return Ds;
            for(int i = 0 ; i < DSSanpham.Count ; i++)
            {        
                if(ManagerFind.getInstance().Contail(DSSanpham[i].TenSanPham,input))
                {
                    Ds.Add(DSSanpham[i]);
                }       
            }
            return Ds;
        }
        public List<Sanpham>Timkiemnangcao(string[]input)
        {
            List<Sanpham> Ds = new List<Sanpham>();
            for (int i = 0; i < DSSanpham.Count; i++ )
            {
                if(CheckFind(DSSanpham[i],input))
                {
                    Ds.Add(DSSanpham[i]);
                }
            }
            return Ds;
        }
        public bool CheckFind(Sanpham a,string[] b)
        {
            List<Find> DsFind = ManagerFind.getInstance().getDSFind();
            int cout = 0;
            for (int i = 0; i < b.Length; i++)
            {
                for(int j = 0 ; j < DsFind.Count ; j++)
                {
                    if(DsFind[j].CheckFind_product(a,b[i]))
                    {
                        cout++;
                        DsFind.RemoveAt(j);
                        break;
                    }
                }
            }
            if (cout == b.Length)
                return true;
            return false;
        }
        public List<Sanpham> Timkiemloai(string input)
        {
            for(int i = 0 ; i < DbLoaisanpham.Count ; i++)
            {
                if(ManagerFind.getInstance().Contail(DbLoaisanpham[i].TenLoai,input))
                {
                    return DSSanpham.Where(m => m.ID_Loai == DbLoaisanpham[i].ID).ToList();
                }
            }
            return new List<Sanpham>();
        }
        public List<Sanpham> TimkiemNSX(string input)
        {
            List<Sanpham> Ds = new List<Sanpham>();
            for (int i = 0; i < DSSanpham.Count; i++)
            {
                if (DSSanpham[i].NSX == null)
                    continue;
                if(ManagerFind.getInstance().Contail(DSSanpham[i].NSX,input))
                {
                    Ds.Add(DSSanpham[i]);
                }
            }
            return Ds;
        }
    }
    public class Comment_List
    {
        public PagingList paginglist;
        public int istintuc;
        public int ID;
    }

    public class PagingList
    {
        public int countpage;
        public int page;
        public int numberonpage;
        public List<Comment> List_comment;
        public PagingList(List<Comment>ds,int Ipage,int Inumberonpage)
        {
            countpage = (int)Math.Ceiling((ds.Count * 1.0) / Inumberonpage);
            page = Ipage;
            numberonpage = Inumberonpage;
            if (page <= countpage)
            {
                List_comment = ds.Skip((page - 1) * numberonpage).Take(numberonpage).ToList();
            }
            else
                List_comment = new List<Comment>();
        }
    }
    public class SanPhamtutao
    {
        public SanPhamtutao()
        {

        }
        public SanPhamtutao(int id,string name,string image,float gt,string mota,List<string> Hinhmota)
        {
            ID = id;
            Tensanpham = name;
            Loai = "Sản phẩm";
            Linkimage = image;
            Giatien = gt;
            Hinhanhmotasanpham = Hinhmota;
            MotaSanpham = mota;
        }

        public SanPhamtutao(int id, string name, string loai, string image, float gt, string mota, List<string> Hinhmota)
        {
            ID = id;
            Tensanpham = name;
            Loai = loai;
            Linkimage = image;
            Giatien = gt;
            Hinhanhmotasanpham = Hinhmota;
            MotaSanpham = mota;
        }
        public int ID { get; set; }
        public string Tensanpham { get; set; }
        public string Linkimage { get; set; }
        public float Giatien { get; set; }

        private List<string> Hinhanhmotasanpham;

        public List<string> HinhanhMota
        {
            get { return Hinhanhmotasanpham; }
            set { Hinhanhmotasanpham = value; }
        }
        public string MotaSanpham { get; set; }

        public string Loai { get; set; }
        
    }
    public class Danhsachtaikhoan
    {
        public string current_sort = "1";
        public PagedList.IPagedList<TaiKhoan> ds_taikhoan;
    }

    public class ChangePasswordViewModel
    {
        [Required]
        [Display(Name = "OldPassword")]
        public string OldPassword { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }
    }
    public class Menupage
    {
        public string Name;
        public int ID;
        public List<Submenu> Ds = new List<Submenu>();
    }
    public class Phieu
    {
        public string Tennguoimua { get; set; }
        public string Tennguoinhan { get; set; }
        public int IDphieu { get; set; }
        public DateTime ngaymua { get;set; }
        public string Ghichu { get; set; }
        public int Soluong { get; set; }
        public double Tongtien { get; set; }
        public int Trangthai { get; set; }
    }
}