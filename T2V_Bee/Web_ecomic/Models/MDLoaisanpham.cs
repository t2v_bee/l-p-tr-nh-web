﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.WebPages.Html;

namespace Web_ecomic.Models
{
    [MetadataTypeAttribute(typeof(LoaiSanPham.Metadata))]
    public partial class LoaiSanPham
    {
        public List<LoaiSanPham> DbLoaisanpham = new List<LoaiSanPham>();

        internal sealed class Metadata
        {

            [Required]
            [Display(Name = "Tên loại sản phẩm")]
            public string TenLoai { get; set; }

        }
    }
}