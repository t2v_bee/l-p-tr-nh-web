﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Web_ecomic.Models
{
    public class AdminModels
    {
    }

    public class ListUser
    {
        public PagedList.IPagedList<TaiKhoan> ds_thongtintaikhoan;
        public List<string> ds_id = new List<string>();
    }

    public partial class Phanquyen
    {
        public PagedList.IPagedList<ApplicationUser> ds_taikhoan;

        [Required]
        [Display(Name = "Email")]
        [RegularExpression("^[a-zA-Z0-9_\\.-]+@([a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$", ErrorMessage = "E-mail is not valid")]
        public string Email { get; set; }
    }
}