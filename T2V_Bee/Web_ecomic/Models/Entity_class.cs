﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Web_ecomic.Models
{
    public partial class TaiKhoan
    {
        public bool IsAdmin { get; set; }
    }
    [MetadataTypeAttribute(typeof(TaiKhoan.Metadata))]
    public partial class TaiKhoan
    {

        internal sealed class Metadata
        {

            [Required]
            [Display(Name = "Tentaikhoan")]
            [StringLength(256, ErrorMessage = "The {0} Phải lớn hơn {2} và nhỏ hơn {1} .", MinimumLength = 6)]
            public string Tentaikhoan { get; set; }
            [Required]
            [Display(Name = "Email")]
            [RegularExpression("^[a-zA-Z0-9_\\.-]+@([a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$", ErrorMessage = "E-mail is not valid")]
            public string Email { get; set; }
        }
    }

    public class ThongTinTaiKhoan
    {
        [Required]
        [Display(Name = "Tentaikhoan")]
        public string Tentaikhoan { get; set; }
        [Required]
        [Display(Name = "Matkhau")]
        [DataType(DataType.Password)]
        public string Matkhau { get; set; }
        [Required]
        [Display(Name = "Email")]
        [RegularExpression("^[a-zA-Z0-9_\\.-]+@([a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$", ErrorMessage = "E-mail is not valid")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Vui lòng chọn giới tính!", AllowEmptyStrings = false)]
        [Display(Name = "Giới tính")]
        public string SelectedSex { get; set; }

        [Required(ErrorMessage = "Vui lòng điền ngày tháng năm sinh!", AllowEmptyStrings = false)]
        [Display(Name = "Ngày tháng năm sinh")]
        public string Date { get; set; }

        [Required(ErrorMessage = "Vui lòng cung cấp nơi ở hiện tại!", AllowEmptyStrings = false)]
        [Display(Name = "Nơi ở hiện tại")]
        public string SelectedAdress { get; set; }

        [Required(ErrorMessage = "Vui lòng cung cấp số điện thoại", AllowEmptyStrings = false)]
        [RegularExpression(@"(09)\d{8}|(01)\d{9}", ErrorMessage = "Entered phone format is not valid.")]
        [Display(Name = "Điện thoại liên lạc")]
        [DataType(DataType.PhoneNumber)]
        public string PhoneNumber { get; set; }

        public int IDtaikhoan { get; set; }
    }

    [MetadataTypeAttribute(typeof(Tintuc.Metadata))]
    public partial class Tintuc
    {

        internal sealed class Metadata
        {

            [Required]
            [Display(Name = "Tittle")]
            [StringLength(200, ErrorMessage = "The {0} Phải lớn hơn {2} và nhỏ hơn {1} .", MinimumLength = 6)]
            public string Tittle { get; set; }
            [Required]
            [Display(Name = "Author")]
            [StringLength(50, ErrorMessage = "The {0} Phải lớn hơn {2} và nhỏ hơn {1} .", MinimumLength = 6)]

            public string Author { get; set; }
            [StringLength(200, ErrorMessage = "The {0} Phải lớn hơn {2} và nhỏ hơn {1} .", MinimumLength = 6)]
            public string Imageurl { get; set; }
            [Required]
            [Display(Name = "Noidung")]
            [StringLength(50, ErrorMessage = "The {0} Phải lớn hơn {2} và nhỏ hơn {1} .", MinimumLength = 8)]
            public string Noidung { get; set; }
        }
    }
    [MetadataTypeAttribute(typeof(Comment.Metadata))]
    public partial class Comment
    {
        internal sealed class Metadata
        {

            [Required]
            [Display(Name = "Name")]
            [StringLength(50, ErrorMessage = "The {0} Phải lớn hơn {2} và nhỏ hơn {1} .", MinimumLength = 2)]
            public string Name { get; set; }
            [Required]
            [Display(Name = "Noidung")]
            [StringLength(100, ErrorMessage = "The {0} Phải lớn hơn {2} và nhỏ hơn {1} .", MinimumLength = 6)]
            public string Noidung { get; set; }
        }
    }
    [MetadataTypeAttribute(typeof(Nguoinhan.Metadata))]
    public partial class Nguoinhan
    {

        internal sealed class Metadata
        {

            [Required]
            [Display(Name = "Hoten")]
            [StringLength(30, ErrorMessage = "The {0} Phải lớn hơn {2} và nhỏ hơn {1} .", MinimumLength = 6)]
            public string Hoten { get; set; }
            [Required]
            [Display(Name = "Diachi")]
            [StringLength(100, ErrorMessage = "The {0} Phải lớn hơn {2} và nhỏ hơn {1} .", MinimumLength = 6)]
            public string Diachi { get; set; }
            [Required]
            [Display(Name = "SDT")]
            [RegularExpression("(0+9+[0-9]{8})|(0+1+[0-9]{9})", ErrorMessage = "Phone number is not valid")]
            public string SDT { get; set; }
        }
    }
}