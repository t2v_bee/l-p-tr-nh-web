﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Web_ecomic.Models
{
    [MetadataTypeAttribute(typeof(Sanpham.Metadata))]
    public partial class Sanpham
    {
        public List<LoaiSanPham> DbLoaisanpham = new List<LoaiSanPham>();

        internal sealed class Metadata
        {

            [Required]
            [Display(Name = "Tên sản phẩm")]
            [StringLength(50, ErrorMessage = "The {0} Phải lớn hơn {2} và nhỏ hơn {1} .", MinimumLength = 6)]
            public string TenSanPham { get; set; }

            [Required]
            [Display(Name = "Giá tiền")]
            public Nullable<double> Giatien { get; set; }

            [Required]
            [Display(Name = "Mô tả sản phẩm")]
            public string Motasanpham { get; set; }
            [Required]
            [Display(Name = "Số lượng")]
            public Nullable<int> Soluong { get; set; }
            [Required]
            [Display(Name = "Nhà sản xuất")]
            [StringLength(30, ErrorMessage = "The {0} Phải lớn hơn {2} và nhỏ hơn {1} .", MinimumLength = 1)]
            public string NSX { get; set; }
            [Required]
            [Display(Name = "Loại")]
            public Nullable<int> ID_Loai { get; set; }

        }
    }
}