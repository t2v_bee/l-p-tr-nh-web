﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Web_ecomic.Models
{
    public class ManagerFind
    {
        private static ManagerFind manager = null;
        private List<Find> Ds_Find = null;
        public static ManagerFind getInstance()
        {
            if(manager == null)
                manager = new ManagerFind();
            return manager;
        }

        public List<Find> getDSFind()
        {
                Ds_Find = new List<Find>();
                Ds_Find.Add(new Find_Name());
                Ds_Find.Add(new Find_NSX());
                Ds_Find.Add(new Find_Loai());
            return Ds_Find;
        }
       
        public bool Contail(string a, string b)
        {
            try
            {
                a = Toiuuhoa(a);
                b = Toiuuhoa(b);
                if (a.Contains(b))
                    return true;
                string[] ds1 = a.Split(' ');
                string[] ds2 = b.Split(' ');
                for (int i = 0; i < ds1.Length; i++)
                {
                    for (int j = 0; j < ds2.Length; j++)
                    {

                        string t1 = ds1[i].ToLower();
                        string t2 = ds2[j].ToLower();
                        if (t1.Contains(t2))
                            return true;
                    }
                }
            }
            catch (Exception e) { }
            return false;
        }
        public string Toiuuhoa(string a)
        {
            string b = a;
            for(int i = 1 ; i < b.Length ; i++)
            {
                if(b[i] == ' ' && b[i - 1] == ' ' )
                {
                    b = b.Remove(i, 1);
                    i--;
                }
            }
            if (b[b.Length - 1] == ' ')
                b = b.Remove(b.Length - 1, 1);
            if (b[0] == ' ')
                b = b.Remove(0, 1);
            return b;
        }
        public bool checkLoai(List<LoaiSanPham>a,string input)
        {
            for(int i = 0 ; i < a.Count ; i++)
            {
                if(Contail(a[i].TenLoai,input))
                {
                    return true;
                }
            }
            return false;
        }
        public bool checkNSX(List<Sanpham> a, string input)
        {
            for (int i = 0; i < a.Count; i++)
            {
                if (Contail(a[i].NSX, input))
                {
                    return true;
                }
            }
            return false;
        }
        public bool checkSP(List<Sanpham> a, string input)
        {
            for (int i = 0; i < a.Count; i++)
            {
                if (Contail(a[i].TenSanPham, input))
                {
                    return true;
                }
            }
            return false;
        }
    }
}